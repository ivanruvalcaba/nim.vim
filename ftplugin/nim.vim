if exists('b:did_ftplugin')
  finish
endif

let b:did_ftplugin = 1
let s:save_cpo = &cpo

set cpo&vim

setlocal formatoptions-=t formatoptions+=croql
setlocal comments=:##,:#
setlocal commentstring=#\ %s
setlocal suffixesadd=.nim
setlocal expandtab  "Make sure that only spaces are used
setlocal foldmethod=indent
setlocal tabstop=2
setlocal softtabstop=2
setlocal shiftwidth=2

let &cpo = s:save_cpo
unlet s:save_cpo

" vim: set et sw=2 ts=2:
