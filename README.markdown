[//]: # (Filename: README.markdown)
[//]: # (Author: Iván Ruvalcaba)
[//]: # (Contact: <ivanruvalcaba[at]disroot[dot]org>)
[//]: # (Created: 08 ago 2020 06:38:53)
[//]: # (Last Modified: 08 ago 2020 07:05:47)

# Nim language support for Vim

This provides [Nim](https://nim-lang.org) language support for Vim:

* Syntax highlighting.
* Auto-indent.

The source of this script comes mainly from https://www.vim.org/scripts/script.php?script_id=2632 and https://github.com/zah/nim.vim.

## Installation

Installing `nim.vim` is easy but first you need to have any plugin manager installed (e.g. *Pathogen*, *Vundle*, *Plug*).

## Authors

* **Iván Ruvalcaba** - *Initial work* - [ivanruvalcaba](https://gitlab.com/ivanruvalcaba).

## License

This project is licensed under the Lesser General Public License (LGPL) version 3 - see the [LICENSE](LICENSE) file for details.
